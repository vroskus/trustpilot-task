import register from 'ignore-styles'
register(['png'])

const config = require('../config.json')
import { expect } from 'chai'
import { mount } from 'enzyme'
import { click } from './helpers/actions'
import AppFactory from '../src/app'

const FakeCommunicator = {
	createNewMazeGame: (data) => {
		return new Promise((resolve) => {
			return resolve(require('./fakeApiResponses/createNewMazeGame.json'))
		})
	},
	getMazeCurrentState: (mazeId) => {
		return new Promise((resolve) => {
			return resolve(require('./fakeApiResponses/getMazeCurrentState.json'))
		})
	},
	makeNextMoveInTheMaze: (mazeId, direction) => {
		return new Promise((resolve) => {
			return resolve(require('./fakeApiResponses/makeNextMoveInTheMaze.json'))
		})
	}
}

describe('Integration test', function() {

	const App = AppFactory({ communicator: FakeCommunicator })
	const wrapper = mount(App)

	it('required value validation is working', function() {
		expect(wrapper.find('#width-helper-text').length).to.equal(0)
		expect(wrapper.find('#height-helper-text').length).to.equal(0)
		expect(wrapper.find('#name-helper-text').length).to.equal(0)

		click(wrapper.find('#start').node)

		expect(wrapper.find('#width-helper-text').length).to.equal(1)
		expect(wrapper.find('#height-helper-text').length).to.equal(1)
		expect(wrapper.find('#name-helper-text').length).to.equal(1)
	})

	it('min value validation is working', function() {
		const underMin = config.mazeSizeLimits.min - 1
		wrapper.find('#width').simulate('change', {target: {value: underMin}})
		wrapper.find('#height').simulate('change', {target: {value: underMin}})

		expect(wrapper.find('#width-helper-text').length).to.equal(1)
		expect(wrapper.find('#height-helper-text').length).to.equal(1)
	})

	it('max value validation is working', function() {
		const overMax = config.mazeSizeLimits.max + 1
		wrapper.find('#width').simulate('change', {target: {value: overMax}})
		wrapper.find('#height').simulate('change', {target: {value: overMax}})

		expect(wrapper.find('#width-helper-text').length).to.equal(1)
		expect(wrapper.find('#height-helper-text').length).to.equal(1)
	})

	it('after valid setup submit maze is rendered, controls visible', function() {
		const validSize = Math.floor(Math.random()*(config.mazeSizeLimits.max-config.mazeSizeLimits.min+1)+config.mazeSizeLimits.min)

		expect(wrapper.find('#maze').length).to.equal(0)
		expect(wrapper.find('#controls').length).to.equal(0)

		wrapper.find('#width').simulate('change', {target: {value: validSize}})
		wrapper.find('#height').simulate('change', {target: {value: validSize}})
		wrapper.find('#name').simulate('change', {target: {value: 'Pinkie Pie'}})

		click(wrapper.find('#start').node)

		setTimeout(() => {
			expect(wrapper.find('#maze').length).to.equal(1)
			expect(wrapper.find('#controls').length).to.equal(1)
		})
	})

})