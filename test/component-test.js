import React from 'react'
import { expect } from 'chai'
import { mount } from 'enzyme'
import ControlsBar from '../src/components/controlsBar'

const data = { 
  buttonsQuantity: 5, 
  hintText: 'Hint: you can use keyboard arrows', 
  direction: null,
  directions: {
    0: 'north',
    1: 'south',
    2: 'west',
    3: 'east',
    4: 'stay'
  } 
}

const moveFunction = input => {
  data.direction = input
}

describe('ControlsBar', function() {

  const wrapper = mount(<ControlsBar move={moveFunction} />)

  it('contains proper quantity of buttons', function() {
    expect(wrapper.find('button').length).to.equal(data.buttonsQuantity)
  })

  it('hint text is visible', function() {
    expect(wrapper.find('p').text()).to.equal(data.hintText)
  })

  for (let id in data.directions) {
    it(`button click returns proper direction (${data.directions[id]})`, function() {
      wrapper.find('button').at(id).simulate('click')
      expect(data.direction).to.equal(data.directions[id])
    })
  }

})