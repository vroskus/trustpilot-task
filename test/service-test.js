import React from 'react'
import { expect } from 'chai'
import rainbowColorGenerator from '../src/services/rainbowColorGenerator'

const data = {
  colorCodeLength: 7
}

describe('RainbowColorGenerator (the rainbow)', function() {

  it('returned color code length is correct', function() {
    expect(rainbowColorGenerator.getColor().length).to.equal(data.colorCodeLength)
  })

  it('returned color begins with #', function() {
    expect(rainbowColorGenerator.getColor().charAt(0)).to.equal('#')
  })

})