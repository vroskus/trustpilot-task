require('babel-register')()
let jsdom = require('jsdom')

let exposedProperties = ['window', 'navigator', 'document']

global.document = jsdom.jsdom(
	'<html><body><div id="root"></div></body></html>',
	{
		url: `file://${__dirname}`,
		virtualConsole: jsdom.createVirtualConsole().sendTo(console)
	}
)

global.window = document.defaultView
global.window.cancalAnimationFrame = () => {}

Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property)
    global[property] = document.defaultView[property]
  }
})

global.navigator = {
  userAgent: 'node.js'
}

const localStorage = {}
global.localStorage = {
	getItem(key) {
		return localStorage.key
	},
	setItem(key, value) {
		localStorage.key = value
	}
}

documentRef = document