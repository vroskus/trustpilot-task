const createMouseEvent = (eventName, eventData) => {
  const options = Object.assign({
    bubbles: true,
    cancelable: (eventName !== 'mousemove'),
    view: global.window,
    detail: 0,
    screenX: 0,
    screenY: 0,
    clientX: 1,
    clientY: 1,
    ctrlKey: false,
    altKey: false,
    shiftKey: false,
    metaKey: false,
    button: 0
  }, eventData)
  
  return new global.window.MouseEvent(eventName, options)
}

const triggerMouseEvent = (element, eventName, eventData) => {
  const event = createMouseEvent(eventName, eventData)
  element.dispatchEvent(event)
}

export const click = element =>
  triggerMouseEvent(element, 'click')