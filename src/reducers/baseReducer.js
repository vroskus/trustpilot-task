export default function reducer(state = {
	layout: {
		preloader: false,
		setupBar: false,
		controlsBar: false,
		maze: false,
		final: false
	},
	finalImage: null,
	validMazeId: false,
	mazeId: null,
	maze: null
}, action) {
	let newState = JSON.parse(JSON.stringify(state))
	switch (action.type) {
		case 'MAZE_ID_FOUND_IN_STORAGE':
			{
				newState.mazeId = action.payload
				break
			}
		case 'MAZE_ID_NOT_FOUND_IN_STORAGE':
			{
				newState.layout.setupBar = true
				break
			}
		case 'GET_MAZE_STATE_PENDING':
			{
				newState.layout.preloader = true
				break
			}
		case 'GET_MAZE_STATE_FULFILLED':
			{
				let gameState = null

				if (action.payload 
					&& action.payload['game-state'] 
					&& action.payload['game-state'].state
				) {
					gameState = action.payload['game-state'].state.toLowerCase()
				}

				if (gameState === 'active') {
					if (action.payload['game-state']['state-result'].toLowerCase() !== "can't walk in there"
						|| newState.maze === null
					) {
						newState.maze = action.payload
					}

					newState.validMazeId = true
					newState.layout.preloader = false
					newState.layout.setupBar = false
					newState.layout.controlsBar = true
					newState.layout.maze = true
				} else if (gameState === 'won' || gameState === 'over') {
					newState.finalImage = action.payload['game-state']['hidden-url']
					newState.layout.final = true
					newState.layout.preloader = false
					newState.layout.setupBar = false
					newState.layout.controlsBar = false
					newState.layout.maze = false
				} else {
					newState.validMazeId = false
					newState.layout.preloader = false
					newState.layout.setupBar = true
					newState.layout.controlsBar = false
					newState.layout.maze = false
					newState.layout.final = false
				}
				break
			}
		case 'GET_MAZE_STATE_REJECTED':
			{
				newState.validMazeId = false
				newState.layout.preloader = false
				newState.layout.setupBar = true
				newState.layout.controlsBar = false
				break
			}
		case 'CREATE_MAZE_PENDING':
			{
				newState.layout.preloader = true
			}
		case 'CREATE_MAZE_FULFILLED':
			{
				newState.mazeId = action.payload
				newState.validMazeId = true
				newState.layout.preloader = false
				newState.layout.setupBar = false
				newState.layout.final = false
				break
			}
		case 'CREATE_MAZE_REJECTED':
			{
				newState.mazeId = null
				newState.validMazeId = false
				newState.layout.preloader = false
				newState.layout.setupBar = false
				break
			}
		case 'RESET_MAZE':
			{
				newState.mazeId = null
				newState.validMazeId = false
				newState.layout.preloader = false
				newState.layout.maze = false
				newState.layout.controlsBar = false
				newState.layout.setupBar = true
				newState.layout.final = false
				break
			}
	}
	return newState
}