import { combineReducers } from 'redux'

import base from './baseReducer'

export default combineReducers({
	base
})