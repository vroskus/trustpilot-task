const config = require('../../config.json')
import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'

const styles = {
  centerContent: {
    textAlign: 'center'
  },
  finalImage: {
    width: '100%'
  }
}

class Final extends React.Component {

  render() {
    const { classes, image } = this.props
    return (
      <div className={classes.centerContent}>
      	<img src={config.serverUrl + image} alt="Final image"  className={classes.finalImage} />
      </div>
    )
  }

}

Final.propTypes = {
  classes: PropTypes.object.isRequired,
  image: PropTypes.string
}

export default withStyles(styles)(Final)