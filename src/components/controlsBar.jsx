import React from 'react'
import PropTypes from 'prop-types'
import { Typography, Grid, Button } from 'material-ui'
import { withStyles } from 'material-ui/styles'
import { grey } from 'material-ui/colors'

import UpIcon from 'material-ui-icons/KeyboardArrowUp'
import DownIcon from 'material-ui-icons/KeyboardArrowDown'
import LeftIcon from 'material-ui-icons/KeyboardArrowLeft'
import RightIcon from 'material-ui-icons/KeyboardArrowRight'
import SpaceIcon from 'material-ui-icons/AllOut'

import Hotkeys from 'react-hot-keys'

const styles = {
  controlsBarHint: {
    textAlign: 'center',
    padding: 15
  },
  controlsBarHintText: {
    color: grey[500]
  }
}

class ControlsBar extends React.Component {

  constructor() {
    super()
    this.action.bind(this)
  }

  action(key) {
    let direction
    switch (key) {
      case 'up':
        direction = 'north'
        break
      case 'down':
        direction = 'south'
        break
      case 'left':
        direction = 'west'
        break
      case 'right':
        direction = 'east'
        break
      case 'space':
        direction = 'stay'
        break
    }
    this.props.move(direction)
  }

  render() {
    const { classes } = this.props
    return (
      <Hotkeys 
        keyName="up,down,left,right,space" 
        onKeyDown={(keyName) => { this.action(keyName) }}
      > 
        <div id="controls" className={classes.controlsBar}>  
          <Grid container layout="row" justify="center" spacing={16}>
            <Grid item>
              <Button fab color="accent" aria-label="move up" onClick={() => { this.action('up') }}>
                <UpIcon />
              </Button>
            </Grid>
            <Grid item>
              <Button fab color="accent" aria-label="move down" onClick={() => { this.action('down') }}>
                <DownIcon />
              </Button>
            </Grid>
            <Grid item>
              <Button fab color="accent" aria-label="move left" onClick={() => { this.action('left') }}>
                <LeftIcon />
              </Button>
            </Grid>
            <Grid item>
              <Button fab color="accent" aria-label="move right" onClick={() => { this.action('right') }}>
                <RightIcon />
              </Button>
            </Grid>
            <Grid item>
              <Button fab color="accent" aria-label="stay" onClick={() => { this.action('space') }}>
                <SpaceIcon />
              </Button>
            </Grid>
          </Grid>
          <div className={classes.controlsBarHint}>
            <Typography className={classes.controlsBarHintText}>Hint: you can use keyboard arrows</Typography>
          </div>
        </div>
      </Hotkeys>
    )
  }

}

ControlsBar.propTypes = {
  classes: PropTypes.object.isRequired,
  move: PropTypes.func
}

export default withStyles(styles)(ControlsBar)