import React from 'react'
import PropTypes from 'prop-types'
import { Table, TableBody, TableRow, TableCell, Avatar } from 'material-ui'
import { withStyles } from 'material-ui/styles'
import rainbowColorGenerator from '../services/rainbowColorGenerator'

import ponyImage from '../images/pony.png'
import domokunImage from '../images/domokun.png'

const mazeBorderStyle = '3px solid '
const slimBorderStyle = '1px solid rgba(235, 235, 235, 1)'

const styles = {
  centerContent: {
    textAlign: 'center'
  },
  table: {
    marginBottom: 15,
    tableLayout: 'fixed',
    borderCollapse: 'separate'
  },
  mazeItem: {
    margin: '0 auto'
  }
}

class Maze extends React.Component {

  renderMaze() {
    const { classes, config } = this.props
    const rows = []
    let cellId = 0

    if (config === null) {
      return
    }

    for (let row = 0; row<config.size[1]; row++) {
      let cells = []
      for (let cell = 0; cell<config.size[0]; cell++) {
        const avatars = []
        const mazeCellStyles = {
          borderRight: slimBorderStyle
        }

        // Setum maze boundries (borders)
        if (row === (config.size[1] - 1)) {
          mazeCellStyles.borderBottom = mazeBorderStyle + rainbowColorGenerator.getColor()
        }
        if (cell === (config.size[0] - 1)) {
          mazeCellStyles.borderRight = mazeBorderStyle + rainbowColorGenerator.getColor()
        }

        config.data[cellId].forEach((cellSide) => {
          if (cellSide === 'north') {
            mazeCellStyles.borderTop = mazeBorderStyle + rainbowColorGenerator.getColor()
          }
          if (cellSide === 'south') {
            mazeCellStyles.borderBottom = mazeBorderStyle + rainbowColorGenerator.getColor()
          }
          if (cellSide === 'west') {
            mazeCellStyles.borderLeft = mazeBorderStyle + rainbowColorGenerator.getColor()
          }
          if (cellSide === 'east') {
            mazeCellStyles.borderRight = mazeBorderStyle + rainbowColorGenerator.getColor()
          }
        })

        // Add pony
        if (config['pony'].indexOf(cellId) !== -1) {
          avatars.push(<Avatar alt="Pony" src={ponyImage} className={classes.mazeItem}></Avatar>)
        }

        // Add dokomun
        if (config['domokun'].indexOf(cellId) !== -1) {
          avatars.push(<Avatar alt="Domokun" src={domokunImage} className={classes.mazeItem}></Avatar>)
        }

        // Add exit
        if (config['end-point'].indexOf(cellId) !== -1) {
          avatars.push(<Avatar className={classes.mazeItem}>E</Avatar>)
        }

        cells.push(<TableCell key={cell} padding="none" style={mazeCellStyles}>{ 
          avatars.length ? avatars[0] : null 
        }</TableCell>)
        cellId++
      }
      rows.push(<TableRow key={row}>{cells}</TableRow>)
    }
    return rows
  }

  render() {
    const { classes, config } = this.props
    return (
      <div className={classes.centerContent}>
      	<Table id="maze" className={classes.table}>
          <TableBody>
            {this.renderMaze()}
          </TableBody>
        </Table>
      </div>
    )
  }

}

Maze.propTypes = {
  classes: PropTypes.object.isRequired,
  config: PropTypes.object
}

export default withStyles(styles)(Maze)