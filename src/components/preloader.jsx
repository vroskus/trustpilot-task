import React from 'react'
import PropTypes from 'prop-types'
import { LinearProgress } from 'material-ui'
import { withStyles } from 'material-ui/styles'

const styles = {
  centerContent: {
    textAlign: 'center'
  }
}

class Preloader extends React.Component {

  render() {
    const { classes } = this.props
    return (
      <div className={classes.centerContent}>
      	<LinearProgress color="accent" />
      </div>
    )
  }

}

Preloader.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Preloader)