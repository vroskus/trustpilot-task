const config = require('../../config.json')
import React from 'react'
import PropTypes from 'prop-types'
import { Typography, Grid, FormControl, TextField, MenuItem, Button } from 'material-ui'
import { withStyles } from 'material-ui/styles'
import { pink, grey } from 'material-ui/colors'

import google from '../services/google'

import PlayIcon from 'material-ui-icons/PlayArrow'
import WarningIcon from 'material-ui-icons/Warning'

import { ValidatorForm } from 'react-form-validator-core'
import { TextValidator} from 'react-material-ui-form-validator'

const styles = {
  centerContent: {
    textAlign: 'center'
  },
  setupBar: {
    padding: 15
  },
  setupBarHintText: {
    color: grey[500]
  },
  setupBarStartButton: {
    margin: '0 auto'
  },
  setupBarWarningBar: {
    backgroundColor: pink[100]
  },
  setupBarWarningBarIcon: {
    color: pink[700]
  }
}

class SetupBar extends React.Component {

  constructor() {
    super()
    this.state = {
      widthValue: '',
      heightValue: '',
      namewidthValue: '',
      difficultyValue: 0,
      validPonyName: google.find('A valid pony name')
    }
  }

  render() {
    const { classes, start, warning } = this.props
    return (
      <div id="setup" className={classes.setupBar}>
        <div className={classes.setupBarHint}>
          <Typography className={classes.setupBarHintText}>Hint: In order to start please setup the challenge correctly</Typography>
        </div>
        <ValidatorForm
          ref="setup"
          onSubmit={() => this.props.start({
            'maze-width': parseInt(this.state.widthValue),
            'maze-height': parseInt(this.state.heightValue),
            'maze-player-name': this.state.nameValue,
            'difficulty': this.state.difficultyValue
          })}
          onError={errors => console.log('Nope, setup is not ready', errors)}
        >
          <Grid container layout="row" alignItems="flex-start">
            <Grid item sm={3} xs={6}>
              <TextValidator
                id="width"
                value={this.state.widthValue}
                onChange={e => this.setState({ 'widthValue': e.target.value })}
                name="width"
                label="Width of the maze"
                margin="normal"
                validators={[
                  'required',
                  'isNumber',
                  'minNumber:' + config.mazeSizeLimits.min,
                  'maxNumber:' + config.mazeSizeLimits.max
                ]}
                errorMessages={[
                  'Width is required',
                  'Width has to be a number',
                  'Should be more than ' + (config.mazeSizeLimits.min - 1),
                  'Should be less than ' + (config.mazeSizeLimits.max + 1)
                ]}
                fullWidth={true}
              />
            </Grid>
            <Grid item sm={3} xs={6}>
              <TextValidator
                id="height"
                value={this.state.heightValue}
                onChange={e => this.setState({ 'heightValue': e.target.value })}
                name="height"
                label="Height of the maze"
                margin="normal"
                validators={[
                  'required',
                  'isNumber',
                  'minNumber:' + config.mazeSizeLimits.min,
                  'maxNumber:' + config.mazeSizeLimits.max
                ]}
                errorMessages={[
                  'Height is required',
                  'Height has to be a number',
                  'Should be more than ' + (config.mazeSizeLimits.min - 1),
                  'Should be less than ' + (config.mazeSizeLimits.max + 1)
                ]}
                fullWidth={true}
              />
            </Grid>
            <Grid item sm={4} xs={10}>
              <TextValidator
                id="name"
                value={this.state.nameValue}
                onChange={e => this.setState({ 'nameValue': e.target.value })}
                name="name"
                label="A valid pony name"
                margin="normal"
                validators={[
                  'required',
                  `matchRegexp:^${this.state.validPonyName}$` 
                ]}
                errorMessages={[
                  'A pony name is required',
                  'No no, google states that it is ' + this.state.validPonyName
                ]}
                fullWidth={true}
              />
            </Grid>
            <Grid item sm={1} xs={2}>
              <TextField
                id="difficulty"
                select
                label="Difficulty"
                name="difficulty"
                value={this.state.difficultyValue}
                onChange={e => this.setState({ 'difficultyValue': e.target.value })}
                margin="normal"
              >
                {[0,1,2,3,4,5,6,7,8,9,10].map(option => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item sm={1} xs={12} className={classes.centerContent}>
              <Button id="start" fab color="accent" aria-label="start pony challenge" className={classes.setupBarStartButton} type="submit">
                <PlayIcon />
              </Button>
            </Grid>
          </Grid>
          { warning ? <Grid className={classes.setupBarWarningBar} layout="row" alignItems="center" container>
            <Grid item>
              <WarningIcon className={classes.setupBarWarningBarIcon} />
            </Grid>
            <Grid item xs>
              <Typography>{ warning }</Typography>
            </Grid>
          </Grid> : '' }
        </ValidatorForm>
      </div>
    )
  }

}

SetupBar.propTypes = {
  classes: PropTypes.object.isRequired,
  start: PropTypes.func,
  warning: PropTypes.any
}

export default withStyles(styles)(SetupBar)