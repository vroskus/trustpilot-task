// Rainbow color generator service
import { red, orange, amber, green, blue, indigo, purple } from 'material-ui/colors'

const rainbowColors = [
	red[500], 
	orange[500], 
	amber[500], 
	green[500], 
	blue[500], 
	indigo[500], 
	purple[500]
]

const getColor = () => {
	return rainbowColors[Math.floor(Math.random() * rainbowColors.length)]
}

const rainbowColorGenerator = {
  getColor
}

export default rainbowColorGenerator