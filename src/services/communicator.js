// Communication between api service

require('isomorphic-fetch')
const config = require('../../config.json')

function get(url) {
  return new Promise(resolve => {
    fetch(url).then(response => {
      resolve(response.json())
    }, onError)
  })
}

function post(url, data) {
  return new Promise(resolve => {
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(response => {
      resolve(response.json())
    }, onError)
  })
}

function onError(response) {
  console.log('Huston, we have a problem...', response)
}

const communicator = {
  createNewMazeGame: (data) => post(config.serverUrl + '/pony-challenge/maze', data),
  getMazeCurrentState: (mazeId) => get(config.serverUrl + '/pony-challenge/maze/' + mazeId),
  makeNextMoveInTheMaze: (mazeId, direction) => post(config.serverUrl + '/pony-challenge/maze/' + mazeId, { direction }),
  getVisualOfTheCurrentStateOfTheMaze: (mazeId) => get(config.serverUrl + '/pony-challenge/maze/' + mazeId + '/print')
}

export default communicator