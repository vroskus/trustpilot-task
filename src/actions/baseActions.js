export const getMazeId = (data) => {
	return (dispatch, getState, services) => {
		return dispatch({
			type: 'CREATE_MAZE',
			payload: services.communicator.createNewMazeGame(data).then(response => {
				const mazeId = response.maze_id
				localStorage.setItem('maze', mazeId)
				return mazeId
			})
		})
	}
}

export const getMazeIdFromLocalStorage = () => {
	const mazeId = localStorage.getItem('maze')
	if (mazeId && mazeId !== 'null') {
		return {
			type: 'MAZE_ID_FOUND_IN_STORAGE',
			payload: mazeId
		}
	}
	return {
		type: 'MAZE_ID_NOT_FOUND_IN_STORAGE',
		payload: null
	}
}

export const getMazeState = (mazeId) => {
	return (dispatch, getState, services) => {
		return dispatch({
			type: 'GET_MAZE_STATE',
			payload: services.communicator.getMazeCurrentState(mazeId)
		})
	}
}

export const makeNextMove = (mazeId, direction) => {
	return (dispatch, getState, services) => {
		return dispatch({
			type: 'MAKE_NEXT_MOVE',
			payload: services.communicator.makeNextMoveInTheMaze(mazeId, direction)
		})
	}
}

export const resetMaze = () => {
	localStorage.setItem('maze', null)
	return {
		type: 'RESET_MAZE',
		payload: true
	}
}