import { applyMiddleware, createStore } from 'redux'

import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'

import reducers from './reducers'

export default (services) => {
	//const middleware = applyMiddleware(promise(), thunk, createLogger())
	const middleware = applyMiddleware(promise(), thunk.withExtraArgument(services))
	return createStore(reducers, middleware)
} 