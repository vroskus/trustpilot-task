import React from 'react'
import { Provider } from 'react-redux'
import createStore from './store'
import Main from './main'
import Communicator from './services/communicator'

const services = {
	'communicator': Communicator
}

export default App = () => (
	<Provider store={createStore(services)}>
    	<Main />
    </Provider>
)
