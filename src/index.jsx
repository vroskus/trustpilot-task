import React from 'react'
import ReactDOM from 'react-dom'
import AppFactory from './app'
import Communicator from './services/communicator'

import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()

const App = AppFactory({ communicator: Communicator })

ReactDOM.render(App, document.getElementById('root'))