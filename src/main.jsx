import React from 'react'
import PropTypes from 'prop-types'
import { AppBar, Toolbar, Typography, Paper, Button } from 'material-ui'
import compose from 'recompose/compose'
import { withStyles } from 'material-ui/styles'
import { connect } from 'react-redux'
import mapDispatchToProps from './actions/dispatchActions'
import { 
  getMazeIdFromLocalStorage, 
  getMazeState, 
  getMazeId, 
  updateVisualComponent, 
  makeNextMove, 
  resetMaze 
} from './actions/baseActions'

import SetupBar from './components/setupBar'
import ControlsBar from './components/controlsBar'
import Maze from './components/maze'
import Final from './components/final'
import Preloader from './components/preloader'
import ResetIcon from 'material-ui-icons/Replay'

const styles = {
  container: {
    maxWidth: 800,
    margin: '0 auto'
  },
  flex: {
    flex: 1
  }
}

class Main extends React.Component {

  start(data) {
    this.props.action(getMazeId(data), () => {
      if (this.props.base.validMazeId) {
        this.props.action(getMazeState(this.props.base.mazeId))
      }
    })
  }

  move(direction) {
    this.props.action(makeNextMove(this.props.base.mazeId, direction), () => {
      if (true) {
        this.props.action(getMazeState(this.props.base.mazeId))
      }  
    })
  }

  componentWillMount() {
    this.props.action(getMazeIdFromLocalStorage(), () => {
      if (this.props.base.mazeId) {
        this.props.action(getMazeState(this.props.base.mazeId))
      }
    })
  }

  render() {
    const { classes, base } = this.props
    return (
      <Paper id="app" className={classes.container}>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography type="title" color="inherit" className={classes.flex}>
              TrustPilot Save The Pony Challenge
            </Typography>
            { !base.layout.setupBar ? <Button 
                fab 
                mini 
                color="accent" 
                aria-label="reset pony challenge" 
                className={classes.button} 
                onClick={() => this.props.action(resetMaze())}
              >
              <ResetIcon />
            </Button> : '' }
          </Toolbar>
        </AppBar>
        { base.layout.setupBar ? <SetupBar start={this.start.bind(this)} warning={null} /> : '' }
        { base.layout.maze ? <Maze config={base.maze} /> : '' }
        { base.layout.final ? <Final image={base.finalImage} /> : '' }
        { base.layout.controlsBar ? <ControlsBar move={this.move.bind(this)} /> : '' }
        { base.layout.preloader ? <Preloader /> : '' }
      </Paper>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    base: state.base,
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default compose(
  withStyles(styles, { name: 'Main' }),
  connect(mapStateToProps, mapDispatchToProps)
)(Main)