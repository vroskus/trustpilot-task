# A challenge
Trustpilot task. Save The Pony Challenge

Task description can be found [here](https://ponychallenge.trustpilot.com)

## System requirements

 - node.js
 - npm
 - git

## Installation

First you have to clone remote project repository
```sh
$ git clone https://bitbucket.org/vroskus/trustpilot-task.git challenge
```

Enter project directory
```sh
$ cd challenge
```

Install all dependencies
```sh
$ npm install
```

Build application files
```sh
$ npm run-script build
```

## Configuration

The configuration file can be found here: `config.json`
File has predefined data, so the application can be run out of the box.

## Running the application

Using any browser open `dist/index.html`

## Running tests

There are several example tests provided

To run tests
```sh
$ npm run-script test
```

## Demo

A working demo can be found [here](http://regattas.eu:8121)

